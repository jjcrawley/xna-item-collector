This repository contains the code written for an item collection game using XNA and MonoGame.
I created this project as part of university module.

The XNA Item Collector folderr contains all the classes written to breathe life into the game.
All the assets were provided by the module tutor.

To Build:
To build this project you'll need MonoGame 3.6 and XNA 4.0 installed on the machine. 
You will also need to have Visual Studio 2015 or later.

You can follow this link for the latest build of the game: https://drive.google.com/open?id=159zcoCcknXoQ-SWd4ZDhrthG-KJH2B8u

Please feel free to contact me should you have any questions.
My email is joshcrawley18@yahoo.co.uk
My site is https://joshcrawley18.wixsite.com/joshuacrawley
 