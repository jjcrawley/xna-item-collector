﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// The enemy manager manages any and all updates for each enemy in the scene
    /// it also handles the creation and spawning of random enemy types within the scene
    /// </summary>
    class EnemyManager : Manager
    {
        List<Enemy> enemyList;              //the list of enemies in the scene
        List<Obstacle> obstacleList;        //the list of obstacles in the scene

        Texture2D ambusherTexture;          //the texture for the ambusher ghost
        Texture2D chaserTexture;            //the texture for the chaser ghost
        SpriteFont hUDFont;                 //the sprite font given to each ghost

        /// <summary>
        /// defualt enemy manager
        /// <remarks>the spawntimer is set to a defualt of 3 seconds for the first enemy</remarks>
        /// </summary>
        public EnemyManager() : base()
        {
            enemyList = new List<Enemy>();
            randomNum = new Random();
            SpawnTimer = 3.0f;            
        }

        /// <summary>
        /// create an enemy manager with predefined bounds that the enemies can spawn within
        /// <remarks>the spawn timer is preset at 3 seconds</remarks>
        /// </summary>
        /// <param name="boundaries">the bounds that the enemy can spawn within</param>
        public EnemyManager(Rectangle boundaries) : base (boundaries)
        {
            enemyList = new List<Enemy>();
            randomNum = new Random();
            SpawnTimer = 3.0f;           
        }

        /// <summary>
        /// update all enemies stored in the enemy manager
        /// </summary>
        /// <param name="gameTime">the current game time</param>
        /// <param name="player">the player</param>
        /// <param name="obstacles">the list of obstacles within the game</param>
        public void Update(GameTime gameTime, Player player, List<Obstacle> obstacles)
        {
            obstacleList = obstacles;       //get up to date object positions

            TimerTick(gameTime);

            //update all the enemies in the list
            foreach (Enemy temp in enemyList)
            {
                temp.Update(player, gameTime);
            }

            //spawn an enemy, if allowed
            if (Timer >= SpawnTimer)
            {
                SpawnRandomEnemy();
                SpawnTimer = randomNum.Next(3, 10);
                ResetTimer();
            }

            //remove any enemies that must be removed
            RemoveEnemy();
        }

        /// <summary>
        /// removes any enemies that have technically died (health below 0)
        /// </summary>
        private void RemoveEnemy()
        {
            //iterate backwards through the list
            for (int i = enemyList.Count - 1; i >= 0; i--)
            {
                if (enemyList[i].Health <= 0)   //if an enemy is 'dead'
                {
                    enemyList.RemoveAt(i);      //remove it
                }
            }
        }

        /// <summary>
        /// spawns a random enemy type
        /// </summary>
        public void SpawnRandomEnemy()
        {
            //stops an enemy from being spawned if the limit is reached
            if (enemyList.Count >= Limit)
            {
                return;
            }

            Enemy newEnemy;
            
            //generate a random number to decide what ghost to create
            if (randomNum.Next(10) < 6)     //spawn an ambusher ghost
            {
                newEnemy = CreateAmbusherGhost();                
            }
            else                //spawn a chaser ghost
            {
                newEnemy = CreateChaserGhost();
            }

            enemyList.Add(newEnemy);
        }


        /// <summary>
        /// creates a chaser ghost
        /// </summary>
        /// <returns>the newly created chaser ghost</returns>
        private ChaserGhost CreateChaserGhost()
        {            
            ChaserGhost newEnemy;

            Vector2 position = new Vector2(0, 0);

            //create the chaser ghost at a predefined position, colour and boundaries
            newEnemy = new ChaserGhost(position, Boundaries, Color.Blue);
            newEnemy.CharacterTexture = chaserTexture;  
            newEnemy.Speed = 2.0f;
            newEnemy.Damage = 4;
            newEnemy.AttackInterval = 2.0f;
            newEnemy.SeekRadius = 200.0f;
            newEnemy.HealthFont = hUDFont;
            newEnemy.Position = RandomisePosition(newEnemy);        //randomises the position of the enemy

            return newEnemy;
        }

        /// <summary>
        /// creates a new ambusher ghost
        /// </summary>
        /// <returns>the newly created ambusher ghost</returns>
        private AmbusherGhost CreateAmbusherGhost()
        {
            AmbusherGhost newEnemy;

            Vector2 position = new Vector2(0, 0);

            //create an ambusher ghost with a predefined position, bounds and colour
            newEnemy = new AmbusherGhost(position, Boundaries, Color.AntiqueWhite);
            newEnemy.AttackRadius = 70.0f;
            newEnemy.CharacterTexture = ambusherTexture;
            newEnemy.Speed = 1.0f;
            newEnemy.Damage = 2;
            newEnemy.AttackInterval = 2.0f;
            newEnemy.SeekRadius = 200.0f;
            newEnemy.HealthFont = hUDFont;
            newEnemy.Position = RandomisePosition(newEnemy);        //randomise the position of the enemy

            return newEnemy;
        }

        /// <summary>
        /// randomises the x and y coordinates of an enemy
        /// </summary>
        /// <param name="newEnemy">the enemy to randomise</param>
        private Vector2 RandomisePosition(Enemy newEnemy)
        {
            Vector2 position;

            do
            {
                //when assigning the position you have to take into account the position that's returned
                //the rectangles position is returned
                //not the centre of the rectangle, which is necessary for proper placement
                position.X = RandomiseXPosition(true, newEnemy.BoundingBox) - newEnemy.BoundingBox.Width / 2.0f;
                position.Y = RandomiseYPosition(true, newEnemy.BoundingBox) - newEnemy.BoundingBox.Height / 2.0f;
                newEnemy.Position = position;
            }
            while (CheckCollisions(newEnemy));

            return position;
        }

        /// <summary>
        /// checks whether the enemy is going to be spawned within another enemy or object
        /// if the enemy is a chaser type, it will check for collisions with walls 
        /// </summary>
        /// <param name="newEnemy">the newly created enemy, who's position you're checking</param>
        /// <returns>true if the enemy is being spawned within another enemy or object</returns>
        private bool CheckCollisions(Enemy newEnemy)
        {
            //check whether the newEnemy is being spawned in another enemy
            foreach (Enemy temp in enemyList)
            {
                if (temp.BoundingBox.Intersects(newEnemy.BoundingBox))
                {
                    return true;
                }
            }

            //if a chaser was created, then check whether its being spawned inside an obstacle
            if (newEnemy.GetType() == typeof(ChaserGhost))
            {
                //check all obstacles in the list
                foreach (Obstacle temp in obstacleList)
                {
                    if (temp.Position.Intersects(newEnemy.BoundingBox))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// draw all the enemies in the enemy manager
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Enemy temp in enemyList)
            {
                temp.Draw(spriteBatch);
            }            
        }               

        /// <summary>
        /// set the ambusher enemy texture 
        /// </summary>
        public Texture2D AmbusherTexture
        {
            set
            {
                ambusherTexture = value;
            }
        }

        /// <summary>
        /// set the HUD font for the enemy
        /// </summary>
        public SpriteFont HUDFont 
        {
            set
            {
                hUDFont = value;
            }
        }

        /// <summary>
        /// set the chaser enemy texture
        /// </summary>
        public Texture2D ChaserTexture
        {
            set
            {
                chaserTexture = value;
            }
        }

        /// <summary>
        /// set the obstacle list
        /// </summary>
        /// <remarks>
        /// this is necessary to keep an up to date list of all obstacles in the scene
        /// </remarks>
        public List<Obstacle> Obstacles
        {
            set
            {
                obstacleList = value;
            }
        }

        /// <summary>
        /// returns a list of all enemies in the manager
        /// </summary>
        public List<Enemy> EnemyList
        {
            get
            {
                return enemyList;
            }
        }
    }
}
