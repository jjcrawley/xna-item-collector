﻿using Microsoft.Xna.Framework;

namespace SOFT144_Assignment
{
    class ChaserGhost : Enemy
    {       
        private float disorientationTime;   //the disorientation time of the enemy

        public ChaserGhost()
        { }

        /// <summary>
        /// create a chaser ghost at a predefined position, with preset bounds and colour
        /// </summary>
        /// <param name="pos">the position of the chaser</param>
        /// <param name="bounds">the boundaries that the chaser can roam within</param>
        /// <param name="col">the colour of the chaser</param>
        public ChaserGhost(Vector2 pos, Rectangle bounds, Color col)
            : base(pos, bounds, col)
        {
            disorientationTime = 2.0f;
            bounced = false;
        }

        /// <summary>
        /// An update method that takes into account when the chaser has been bounced
        /// </summary>
        /// <param name="player">the player</param>
        /// <param name="gameTime">current game time</param>
        public override void Update(Player player, GameTime gameTime)
        {
            if (bounced)        //if the chaser has been bounced
            {
                if (timer == 0 || attackedPlayer)     //if the timer hasn't been started, or the player has been attacked
                {
                    StartTimer(gameTime);
                    attackedPlayer = false;
                }

                bounced = TimerStillTicking(disorientationTime);    //reassign the bounce value
                TimerTick(gameTime);

                Wander();
            }
            else
            {
                base.Update(player, gameTime);      //else defualt update behaviour
            }            

            UpdateHealthHUD();

            WrapViewPort();
        }

        /// <summary>
        /// attacks the player when the ghost collides with him 
        /// </summary>
        /// <param name="player">the player</param>
        /// <param name="gameTime">the current game time</param>
        public override void AttackPlayer(Player player, GameTime gameTime)
        {
            if (BoundingBox.Intersects(player.BoundingBox))
            {
                base.AttackPlayer(player, gameTime);
            }
        }

        /// <summary>
        /// set and retrieve the disorientation time of the chaser
        /// </summary>
        public float DisorientationTime
        {
            set
            {
                disorientationTime = value;
            }
            get
            {
                return disorientationTime;
            }
        }
    }
}
