﻿using Microsoft.Xna.Framework;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the health pick up class
    /// this object will heal the player
    /// </summary>
    class HealthPickUp : Item
    {
        private int heal;       //the amount the item will heal the player by

        public HealthPickUp()
        { }

        /// <summary>
        /// create a health pick up at a custom location
        /// <remarks>the defualt health restored is 5</remarks>
        /// </summary>
        /// <param name="position">the custom location</param>
        public HealthPickUp(Rectangle position) 
            : base(position) 
        {
            heal = 5;          //defualt heal value        
        }

        /// <summary>
        /// heals the player
        /// </summary>
        /// <param name="player">the player who's being healed</param>
        protected override void PerformItemPurpose(Player player)
        {
            player.Health += heal; 	
        }       

        /// <summary>
        /// set the amount you want to heal the player by
        /// </summary>
        public int Heal
        {
            set
            {
                heal = value;
            }
        }
        
    }
}
