﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the item class
    /// </summary>
    class Item
    {
        private Texture2D itemTexture;      //the item texture
        private Rectangle position;         //the position of the item
        private Color itemColour;           //the colour of the item

        private bool collided;      //whether or not the player has interacted with the item
        
        public Item()
        { 
        }

        /// <summary>
        /// create a new item at a specified location
        /// </summary>
        /// <param name="objectPosition">the location</param>
        public Item(Rectangle objectPosition)
        {
            position = objectPosition;            
            collided = false;             
        }

        /// <summary>
        /// an update method for any and all items
        /// </summary>
        /// <param name="player">the player that the update is affecting</param>
        public virtual void Update(Player player)
        {
            if (position.Intersects(player.BoundingBox))
            {
                PerformItemPurpose(player);
                collided = true;                               
            }
        }

        /// <summary>
        /// performs the items specified purpose
        /// </summary>
        /// <param name="player">the player the item is affecting</param>
        protected virtual void PerformItemPurpose(Player player)
        {   //default item behaviour, do nothing
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(itemTexture, position, itemColour); 
        }

        /// <summary>
        /// set and get the item texture
        /// </summary>
        public Texture2D ItemTexture
        {
            set
            {
                itemTexture = value;
            }
            get
            {
                return itemTexture;
            }
        }

        /// <summary>
        /// set and retrieve the position rectangle of the item
        /// </summary>
        public Rectangle Position
        {
            set
            {
                position = value;
            }
            get
            {
                return position;
            }
        }

        /// <summary>
        /// set the item colour
        /// </summary>
        public Color ItemColour
        {
            set
            {
                itemColour = value;
            }
        }

        /// <summary>
        /// retrieve the objects collision state
        /// </summary>
        public bool Collided
        {
            get
            {
                return collided;
            }
        }
    }
}
