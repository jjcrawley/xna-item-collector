﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the enemy class
    /// enemies are capable of tracking and attacking the player
    /// their defualt movement behaviour is wander
    /// </summary>
    class Enemy : Character
    {    
        private Vector2 wanderPosition;     //the wander position, used for defualt wander behvaiour

        protected bool attackedPlayer;      //monitors whether or not the enemy has attacked the player
        protected int damage;               //the damage the enemy deals
        protected float attackInterval;     //the attack interval of the enemy, how often they attack
        protected float timer;              //a timer, used when attacking the player and bouncing
       
        protected float seekRadius;         //the seek radius of the enemy

        private Random randomNum;

        private TextBox healthHUD;          //the health HUD textbox element for the enemy       

        public Enemy()
        { }

        /// <summary>
        /// an enemy class with a custom position, wandering boundaries and colour
        /// </summary>
        /// <param name="pos">the position of the enemy</param>
        /// <param name="roamBounds">the wandering boundaries of the enemy</param>
        /// <param name="enemyCol">the enemy colour</param>
        public Enemy(Vector2 pos, Rectangle roamBounds, Color enemyCol) 
            : base (roamBounds, pos)
        {
            orientation = 3.0f;       

            RotationSpeed = 0.2f;
            TurnFactor = 0.75f;           

            randomNum = new Random();

            CharacterColour = enemyCol;

            currentHealth = 10;
            damage = 2;           

            healthHUD = new TextBox("Health: " + currentHealth);
            healthHUD.Scale = 0.7f;
        }

        /// <summary>
        /// defualt update behaviour for enemies    
        /// </summary>
        /// <param name="player">the player</param>
        /// <param name="gameTime">the current game time</param>
        public virtual void Update(Player player, GameTime gameTime)
        {
            Vector2 steering = player.Position - Position;  //calculate the direction to steer in   

            if (attackedPlayer)    //attacked the player, defualt behaviour is do nothing
            {
                if (TimerStillTicking(attackInterval))  //if the enemy is still 'recovering'
                {
                    TimerTick(gameTime);
                }
                else if (timer == 0)            //if the timer hasn't been started
                {
                    StartTimer(gameTime);
                }
                else                            //reset the timer and recover
                {
                    ResetTimer();
                    attackedPlayer = false;
                }
            }
            else if (steering.Length() <= seekRadius)       //seek the player if its within the seek radius
            {
                //Console.WriteLine("Targetting player");
                if (steering != Vector2.Zero)
                {
                    steering.Normalize();
                }

                PursueTarget(steering);     //use the steering vector to aim the enemy and pursue the player

                AttackPlayer(player, gameTime); //attack the player
            }
            else
            {
                Wander();
            }

            UpdateHealthHUD();

            WrapViewPort();
        }

        /// <summary>
        /// updatest the health hud of the enemy
        /// </summary>
        public void UpdateHealthHUD()
        {
            Vector2 tempPos;    

            tempPos = new Vector2(position.X - 40, position.Y - 35);    //calculate the new position

            healthHUD.Position = tempPos;       //set the position
            //Console.WriteLine("Position: " + healthHUD.Position);

            healthHUD.Text = "Health: " + currentHealth;    //update the text
        }

        /// <summary>
        /// Is the timer still ticking over
        /// </summary>
        /// <param name="timerStop">the timer value for the timer to register</param>
        /// <param name="gameTime">the current game time</param>
        /// <returns>true if the timer is still ticking</returns>
        protected bool TimerStillTicking(float timerStop)
        {
            if (timer > 0 && timer <= timerStop)
            {               
                return true;
            }
            else
            {               
                return false;
            }             
        }

        /// <summary>
        /// resets the timer and starts it off
        /// </summary>
        /// <param name="gameTime">current game time</param>
        protected void StartTimer(GameTime gameTime)
        {
            ResetTimer();
            TimerTick(gameTime);           
        }

        /// <summary>
        /// resets the timer
        /// </summary>
        protected void ResetTimer()
        {
            timer = 0; 
        }

        /// <summary>
        /// makes the timer tick
        /// </summary>
        /// <param name="gameTime">current game time</param>
        protected void TimerTick(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds; 
        }

        /// <summary>
        /// attack the player
        /// </summary>
        /// <param name="player">the player to attack</param>
        /// <param name="gameTime">the current gametime, used to start the timer</param>
        public virtual void AttackPlayer(Player player, GameTime gameTime)
        {
            player.Health -= damage;
            Health -= damage;
            StartTimer(gameTime);
            attackedPlayer = true;
        }

        /// <summary>
        /// causes the enemy to actively pursue its target
        /// </summary>
        /// <param name="steering">the steering vector, tells the enmey which direction it has to go</param>
        private void PursueTarget(Vector2 steering)
        {
            orientation = FaceTarget(steering, orientation, turnFactor * rotationSpeed);        //get the required orientation

            CalculateHeading();         //calculate new heading

            position += heading * speed;            //update position
        }        

        /// <summary>
        /// defualt wandering behaviour, causes the enemy to wander aimlessly
        /// </summary>
        public void Wander()
        {
            float wanderLimit = 0.5f;

            float tempTurnFactor = 0.15f;       //a temporary turn factor

            //calculate a position to wander to
            wanderPosition.X += MathHelper.Lerp(-wanderLimit, wanderLimit, (float)randomNum.NextDouble());
            wanderPosition.Y += MathHelper.Lerp(-wanderLimit, wanderLimit, (float)randomNum.NextDouble());

            if (wanderPosition != Vector2.Zero)
            {
                wanderPosition.Normalize();
            }

            //calculate a new orientation for the enemy
            orientation = FaceTarget(wanderPosition, orientation, tempTurnFactor * rotationSpeed);      //calculate the new orientation

            heading = OrientationAsVector(orientation);     //calculate the new heading

            position += heading * 0.5f * speed; //update position
        }

        /// <summary>
        /// calculates an orientation that will cause the enemy to face its target
        /// </summary>
        /// <param name="steering">the vector it must face towards</param>
        /// <param name="currentOrientation">the current orientation of the enemy</param>
        /// <param name="turnSpeed">how fast the enemey is allowed to turn</param>
        /// <returns>the orientation it must use</returns>
        protected float FaceTarget(Vector2 steering, float currentOrientation, float turnSpeed)
        {
            float newOrientation;       //the new orientation
            float targetOrientation;    //the orientation that it wants
            float orientationDifference;    //the orienation that it's allowed to turn

            targetOrientation = (float)Math.Atan2(steering.Y, steering.X);

            orientationDifference = targetOrientation - currentOrientation; //calculate the orientation difference

            orientationDifference = WrapAngle(orientationDifference);       //wrap the orientation difference

            orientationDifference = MathHelper.Clamp(orientationDifference, -turnSpeed, turnSpeed);

            //use the curernt orientation and orientation difference to calculate the new orientation
            newOrientation = WrapAngle(currentOrientation + orientationDifference);     

            return newOrientation;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {           
            healthHUD.Draw(spriteBatch);
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// set the seek radius of the enemy
        /// </summary>
        public float SeekRadius
        {
            set
            {
                seekRadius = value;
            }
        }       

        /// <summary>
        /// set or retrieve the damage of the enemy
        /// </summary>
        public int Damage
        {
            set
            {
                damage = value;
            }
            get
            {
                return damage;
            }
        }

        /// <summary>
        /// set the enemies attack interval
        /// </summary>
        public float AttackInterval
        {
            set
            {
                attackInterval = value;
            }
        }

        /// <summary>
        /// set the font for UI displays
        /// </summary>
        public SpriteFont HealthFont
        {
            set
            {
                healthHUD.Font = value;
            }
        }
    }
}
