using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SOFT144_Assignment
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;        

        KeyboardState keyPressed;               //the current keys being pressed

        Player player;                          //the player object of the game

        ObstacleManager obstacleManage;         //manages all obstacles in the scene

        ItemManager itemManage;                 //manages all items in the scene

        EnemyManager enemyManage;               //manages all enemies in the scene

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;

            Rectangle viewPortBounds;

            // TODO: Add your initialization logic here

            Vector2 tempPosition = new Vector2();   //use a temporary position for assigning positions
            viewPortBounds = new Rectangle();       //the viewport bounds

            //get the width and height of the viewport
            viewPortBounds.Width = GraphicsDevice.Viewport.Width;   
            viewPortBounds.Height = GraphicsDevice.Viewport.Height;

            //create the obstacle manager with predefined bounds
            obstacleManage = new ObstacleManager(viewPortBounds);
            obstacleManage.Height = 20;        //assign height of obstacles
            obstacleManage.Width = 100;          //assign the width of the obstacles
            obstacleManage.DeleteTimer = 10.0f; //assign the delete timer for obstacles
            obstacleManage.Limit = 5;           //assign the number of obstacles in the scene
            obstacleManage.MaxObstacleSpeed = 4.0f;    //assign the maximum obstacle speed

            //create the item manager with predefined bounds
            itemManage = new ItemManager(viewPortBounds);
            itemManage.Limit = 10;              //limit of items in scene
            itemManage.Size = 25;               //the size of items in the scene

            //create the enemey manager
            enemyManage = new EnemyManager(viewPortBounds);
            enemyManage.Limit = 5;              //set the limit

            tempPosition.X = (float)(viewPortBounds.Width * 0.5);
            tempPosition.Y = (float)(viewPortBounds.Height * 0.5);

            //create the player object at a specified location, with predefined bounds
            player = new Player(tempPosition, viewPortBounds);
            player.Scale = 0.55f;                   //set the scale
    
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //assign character textures and fonts
            player.CharacterTexture = Content.Load<Texture2D>("images//rocketsprite3");
            player.HUDFonts = Content.Load<SpriteFont>("Fonts//UIFont"); 

            obstacleManage.ObstacleTexture = Content.Load<Texture2D>("images//squareImage");
          
            //assign enemy textures and fonts
            enemyManage.AmbusherTexture = Content.Load<Texture2D>("images//chaseCharacterSmall");
            enemyManage.ChaserTexture = Content.Load<Texture2D>("images//chaseCharacterSmall");
            enemyManage.HUDFont = Content.Load<SpriteFont>("Fonts//UIFont");

            //assign item textures
            itemManage.WealthTexture = Content.Load<Texture2D>("images//ball");
            itemManage.HealthTexture = Content.Load<Texture2D>("images//ball");                  

           // TODO: use this.Content to load your game content here
        }       

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {           
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || player.Health == 0)
            {
                this.Exit();
            }

            // TODO: Add your update logic here
            keyPressed = Keyboard.GetState();

            obstacleManage.Update(player, enemyManage.EnemyList, gameTime);                

            player.Update(keyPressed);            

            enemyManage.Update(gameTime, player, obstacleManage.ObstacleList);

            itemManage.Update(player, gameTime);           

            base.Update(gameTime);
        }         

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            itemManage.Draw(spriteBatch);                                       
            enemyManage.Draw(spriteBatch);
            obstacleManage.Draw(spriteBatch);            
            player.Draw(spriteBatch);
            
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
