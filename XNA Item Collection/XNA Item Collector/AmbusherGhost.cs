﻿using Microsoft.Xna.Framework;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the ambusher ghost class
    /// attacks the player when in a specified attack radius 
    /// they don't interact with obstacles
    /// </summary>
    class AmbusherGhost  : Enemy
    {
        private float attackRadius;     //the attack radius of the ambusher ghost

        /// <summary>
        /// create a defualt ambusher ghost
        /// </summary>
        public AmbusherGhost()
            : base()
        { }

        /// <summary>
        /// create an ambusher ghost with custom roam boundaries, position, colour and attack radius
        /// </summary>
        /// <param name="pos">the position of the ambusher</param>
        /// <param name="bounds">the roaming boundaries</param>
        /// <param name="col">colour</param>
        /// <param name="attackRad">the attack radius</param>
        public AmbusherGhost(Vector2 pos, Rectangle bounds, Color col, float attackRad)
            : base(pos, bounds, col)
        {
            AttackRadius = attackRad;
        }

        /// <summary>
        /// create an ambusher ghost with custom wondering boundaries, position and colour
        /// </summary>
        /// <param name="pos">the position of the ambusher</param>
        /// <param name="bounds">the roaming boundaries</param>
        /// <param name="col">the colour</param>
        public AmbusherGhost(Vector2 pos, Rectangle bounds, Color col)
            : base(pos, bounds, col)
        {  }

        /// <summary>
        /// attacks the player if the player is within the attack radius
        /// </summary>
        /// <param name="player">the player</param>
        /// <param name="gameTime">current game time</param>
        public override void AttackPlayer(Player player, GameTime gameTime)
        {
            Vector2 distance = player.Position - Position;      //calculate the distance of the ghost from the player

            if (distance.Length() < attackRadius)       //if the player is in range, attack
            {
                base.AttackPlayer(player, gameTime);
            }
        }       

        /// <summary>
        /// set the attack radius of the ambusher
        /// entering 0 will cause the attack radius to be set to the default: 70
        /// </summary>
        public float AttackRadius
        {
            set
            {
                attackRadius = value;

                if (attackRadius < 0)
                {
                    attackRadius *= -1;
                }
                else if (attackRadius == 0)
                {
                    attackRadius = 70.0f;       //default attack radius
                }
            } 
        }
    }
}
