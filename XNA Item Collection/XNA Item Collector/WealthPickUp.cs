﻿using Microsoft.Xna.Framework;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the wealth pick up class
    /// this class gives money to the player
    /// </summary>
    class WealthPickUp : Item
    {
        private int money;          //the amount of money that the object provides

        public WealthPickUp()
        { }

        /// <summary>
        /// create a wealth pick up at a specified location
        /// <remarks>the defualt money given is 10</remarks>
        /// </summary>
        /// <param name="position">the specified location</param>
        public WealthPickUp(Rectangle position)
            : base(position)
        {
            money = 10;     //default money value
        }

        /// <summary>
        /// gives wealth to the player
        /// </summary>
        /// <param name="player">the player the pickup is affecting</param>
        protected override void PerformItemPurpose(Player player)
        {
            player.Wealth += money;            
        }       

        /// <summary>
        /// set the amount of money you want to give the player
        /// </summary>
        public int Money
        {
            set
            {
                money = value;
            }
            get
            {
                return money;
            }
        }
    }
}
