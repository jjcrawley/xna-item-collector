﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    class Obstacle
    {
        private Rectangle position;             //the position of the obstacle

        private Texture2D obstacleTexture;      //the texture of the obstacle
        private Color obstacleColour;           //the colour of the obstacle
        private Vector2 direction;              //the direction that the obstacle must move

        private float speed;                    //the speed that the obstacle moves at
        private float timer;                    //a local timer for the obstacle

        /// <summary>
        /// create an obstacle with a custom size and movement vector
        /// </summary>
        /// <param name="obstacle">the custom obstacle rectangle</param>
        /// <param name="movementDirection">custom movement vector</param>
        public Obstacle(Rectangle obstacle, Vector2 movementDirection)
        {
            position = obstacle;
            obstacleColour = Color.Red;
            direction = movementDirection;
            timer = 0;
        }

        /// <summary>
        /// update method for the obstacle
        /// </summary>
        /// <param name="gameTime">the current game time</param>
        public void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;  //add time to the timer

            //update the position
            position.X += (int)(direction.X * speed);           
            position.Y += (int)(direction.Y * speed);                                                           
        }

        /// <summary>
        /// reverse the direction of the obstacle
        /// </summary>
        public void ReverseDirection()
        {
            direction *= -1;
        }       

        /// <summary>
        /// checks for collisions with a character in the scene
        /// </summary>
        /// <param name="toCheck">the character to check</param>
        public void CheckForCollision(Character toCheck)
        {
            if (position.Intersects(toCheck.BoundingBox))
            {
                //toCheck.Bounce();
                //toCheck.Bounce(position);
                //toCheck.Bounce(position.Center);
                toCheck.Collide(position);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(obstacleTexture, position, obstacleColour);
        }

        /// <summary>
        /// set the obstacle texture
        /// </summary>
        public Texture2D ObstacleTexture
        {
            set
            {
                obstacleTexture = value;
            }
        }

        /// <summary>
        /// set and retrieve the position of the obstacle
        /// </summary>
        public Rectangle Position
        {
            set
            {
                position = value;
            }
            get
            {
                return position;
            }
        }

        /// <summary>
        /// retrieve the current timer value
        /// </summary>
        public float Timer
        {            
            get
            {
                return timer;
            }
        }

        /// <summary>
        /// set the speed of the obstacle, value can't be negative
        /// </summary>
        public float Speed
        {
            set
            {
                if (speed < 0)
                {
                    speed = 0;
                }
                else
                {
                    speed = value;
                }
            }        
        }
    }
}
