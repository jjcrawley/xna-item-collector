﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the item manager for the game
    /// this class updates all the items in the game 
    /// it also generates new items to be placed in the scene
    /// </summary>
    class ItemManager : Manager
    {
        List<Item> itemList;            //the list of items in the scene
       
        Texture2D healthTexture;        //the texture for the health item
        Texture2D wealthTexture;        //the texture for the wealth item        

        int size;       //the size of the items to be spawned        

        public ItemManager()
        {
            itemList = new List<Item>();
            randomNum = new Random();
            Limit = 10;
        }

        /// <summary>
        /// creates an item manager with a maximum range
        /// </summary>
        /// <param name="viewPort">the rectangle the items are allowed to generate in</param>
        public ItemManager(Rectangle viewPort) : base(viewPort)
        {
            itemList = new List<Item>();
            size = 40;
            randomNum = new Random();
            Limit = 10;
        }

        /// <summary>
        /// allows you to set the bounds and size of the items that will be spawned
        /// </summary>
        /// <param name="boundaries">the rectangle the items are allowed to generate in</param>
        /// <param name="size">the size of the items being spawned</param>
        public ItemManager(Rectangle boundaries, int size) : base(boundaries)
        {            
            itemList = new List<Item>();
            Size = size;
            Limit = 10;
        }

        /// <summary>
        /// updates all the items in the item manager
        /// </summary>
        /// <param name="player">the player</param>
        /// <param name="gameTime">the game time</param>
        public void Update(Player player, GameTime gameTime)
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                itemList[i].Update(player); 
            }

            TimerTick(gameTime);

            if (Timer >= SpawnTimer)        //spawn an item, if allowed
            {
                SpawnRandomItem();
                //Console.WriteLine("spawned random item: " + spawnTimer + " at time: " + timer);
                SpawnTimer = randomNum.Next(2, 5);
                ResetTimer();                
            }

            RemoveItem();            
        }

        /// <summary>
        /// Remove any item from the scene that the player has collided with
        /// </summary>
        private void RemoveItem()
        {
            for (int i = itemList.Count - 1; i >= 0; i--)
            {
                if (itemList[i].Collided)
                {
                    itemList.RemoveAt(i);
                }
            } 
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                itemList[i].Draw(spriteBatch);
            }
        }

        /// <summary>
        /// spawns a random item somewhere in the scene
        /// </summary>
        public void SpawnRandomItem()
        {
            //stops items from spawning when the limit is reached
            if (itemList.Count >= Limit)
            {
                return;
            }
           
            Item newItem;
            Rectangle position;

            int random;            

            random = randomNum.Next(1, 10);            

            position = new Rectangle(0, 0, size, size);

            //choose the item to generate
            if (random < 4)    //health item
            {
                newItem = SpawnHealthPickUp(RandomisePosition(position));                               
            }
            else            //wealth item
            {
                newItem = SpawnWealthPickUp(RandomisePosition(position));                
            }

            itemList.Add(newItem);
        }

        /// <summary>
        /// randomises the position of a target rectangle 
        /// </summary>
        /// <param name="position">the target rectangle to randomise</param>
        /// <returns>the repositioned rectangle</returns>
        private Rectangle RandomisePosition(Rectangle position)
        {
            do
            {
                position.X = RandomiseXPosition(true, position);
                position.Y = RandomiseYPosition(true, position);
            }
            while (CheckCollisions(position));       //stops an item from being generated on top of another item

            return position;
        }

        /// <summary>
        /// creates a health pickup at a specified location
        /// </summary>
        /// <param name="position">the target rectangle to place the item in</param>
        /// <returns>the newly created health pickup</returns>
        private HealthPickUp SpawnHealthPickUp(Rectangle position)
        {
            HealthPickUp newItem;

            //create a new health pickup at a specified location
            newItem = new HealthPickUp(position);
            newItem.ItemTexture = healthTexture;
            newItem.Heal = 7;
            newItem.ItemColour = Color.Violet;

            return newItem;
        }

        /// <summary>
        /// creates a new wealth pickup at a specified location
        /// </summary>
        /// <param name="position">the location rectangle</param>
        /// <returns>the wealth pickup</returns>
        private WealthPickUp SpawnWealthPickUp(Rectangle position)
        {
            WealthPickUp newItem;
            int randomAmount;

            //create a new wealth pickup at a specified location
            newItem = new WealthPickUp(position);
            newItem.ItemTexture = wealthTexture;

            randomAmount = randomNum.Next(1, 20);

            //decide what colour and amount the money item will have
            //the colour of the item reflects the amount of wealth given
            if (randomAmount < 7)   //regular bronze
            {
                newItem.ItemColour = Color.SandyBrown;      //didn't have bronze, next best thing
                newItem.Money = 5;
            }
            else if (randomAmount < 15)     //silver wealth item
            {
                newItem.ItemColour = Color.Silver;
                newItem.Money = 10;
            }
            else                            //rare gold pickup
            {
                newItem.ItemColour = Color.Gold;
                newItem.Money = 15;
            }           
            
            return newItem;
        }        

        /// <summary>
        /// checks whether the newly spawed item will interesct with any other spawned items
        /// </summary>
        /// <param name="check">the newly created item</param>
        /// <returns>true if there are any intersections</returns>
        private bool CheckCollisions(Rectangle check)
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                if(itemList[i].Position.Intersects(check))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// set the size of the items to be spawned
        /// <remarks>if the value is less than zero, the defualt size, 20, will be used</remarks>
        /// </summary>
        public int Size
        {
            set
            {
                if (size < 0)
                {
                    size = 20;
                }
                else
                {
                    size = value;
                }
            }
        }

        /// <summary>
        /// set the texture for health pickups
        /// </summary>
        public Texture2D HealthTexture
        {
            set
            {
                healthTexture = value;
            }
        }

        /// <summary>
        /// set the texture for wealth pickups
        /// </summary>
        public Texture2D WealthTexture
        {
            set
            {
                wealthTexture = value;
            }
        }
    }
}
