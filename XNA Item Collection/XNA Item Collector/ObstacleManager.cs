﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the obstacle manager class
    /// creates and maintains a list of all obstacles in the scene
    /// spawns new obstacles and assigns a random speed and position to newly created obstacles
    /// </summary>
    class ObstacleManager : Manager
    {
        private int height;                 //the height of obstacles to be generated
        private int width;                  //the width of obstacles to be generated
        private float deleteTime;           //the time when obstacles will be removed from the scene
        private float maxObstacleSpeed;     //the max random speed of any created obstacles

        private List<Obstacle> obstacles;   //the list of obstacles in the scene

        private Texture2D obstacleTexture;  //the obstacles texture

        public ObstacleManager() : base()
        {
            obstacles = new List<Obstacle>();
        }

        /// <summary>
        /// create an obstacle manager with custom boundaries
        /// </summary>
        /// <param name="bounds">the custom boundaries</param>
        public ObstacleManager(Rectangle bounds)
            : base(bounds)
        {
            obstacles = new List<Obstacle>();
            Limit = 5;
            deleteTime = 5.0f;
        }

        /// <summary>
        /// updates all the obstacles in the obstacle manager
        /// </summary>
        /// <param name="player">the player</param>
        /// <param name="enemies">list of enemies in scene</param>
        /// <param name="gameTime">current game time</param>
        public void Update(Player player, List<Enemy> enemies, GameTime gameTime)
        {
            //update all obstacles
            foreach (Obstacle temp in obstacles)    
            {
                temp.Update(gameTime);
                UpdateDirection(temp);
            }

            TimerTick(gameTime);

            //spawn another obstacle if allowed
            if (Timer >= SpawnTimer)
            {
                SpawnRandomObstacle();
                SpawnTimer = randomNum.Next(2, 3);
                ResetTimer();
            }
            
            UpdateWallCollisions(player);   //update all obstacles with player collision data

            UpdateWallCollisions(enemies);  //update all obstacles with enemy collision data

            DeleteObstacles();              //delete any obsolete obstacles
        }

        /// <summary>
        /// spawns a random obstacle type, horizontal or vertical, then it adds it to the list for updating and drawing
        /// </summary>
        public void SpawnRandomObstacle()
        {
            //if the limit has been reached
            if (obstacles.Count >= Limit)   
            {
                return; 
            }

            Obstacle newObstacle;

            //generate a random number and create a randomly moving obstacle
            if (randomNum.Next(10) < 5)     //generate an obstacle that moves horizontally
            {
                newObstacle = SpawnHorizontalMovingObstacle();
            }
            else                //generate an obstacle that moves vertically
            {
                newObstacle = SpawnVerticalMovingObstacle();
            }
        
            newObstacle.ObstacleTexture = obstacleTexture;

            //assign a random speed less than the max speed
            newObstacle.Speed = randomNum.Next(1, (int)maxObstacleSpeed + 1); 
     
            obstacles.Add(newObstacle);
        }        

        /// <summary>
        /// creates a new horizontal moving obstacle
        /// positions itself on the left or right edge of the boundaries
        /// </summary>
        /// <returns>the newly created obstacle</returns>
        public Obstacle SpawnHorizontalMovingObstacle()
        {
            Obstacle horizontalObstacle;
            
            Rectangle position;
            Vector2 direction;            
                        
            direction = new Vector2(1, 0);  //move from left to right 

            position = RandomlyOrientRectangle();            

            position.X = RandomiseXPosition(false, position);   //lock to left or right edge

            position.Y = RandomiseYPosition(true, position);    //generate random values within y bounds         

            //generate an obstacle at the specified target rectangle and with a specified direction
            horizontalObstacle = new Obstacle(position, direction);            

            return horizontalObstacle;
        }

        /// <summary>
        /// creates a new obstacle that will move vertically
        /// positions itself at the top or bottom edge of the boundaries
        /// </summary>
        /// <returns>the newly created obstacle</returns>
        public Obstacle SpawnVerticalMovingObstacle()
        {
            Obstacle verticalObstacle;

            Rectangle position;
            Vector2 direction;
            
            direction = new Vector2(0, 1);  //starts off heading downwards

            position = RandomlyOrientRectangle();           

            position.X = RandomiseXPosition(true, position);    //generate numbers within the x bounds

            position.Y = RandomiseYPosition(false, position);   //lock to top or bottom edge

            //generate a new obstacle at a specified target recangle and direction 
            verticalObstacle = new Obstacle(position, direction);           

            return verticalObstacle;
        }

        /// <summary>
        /// creates a rectangle that is randomly oriented
        /// </summary>
        /// <returns>the randomly oriented rectangle</returns>
        private Rectangle RandomlyOrientRectangle()
        {
            Rectangle position;

            //generate a random number and orient accordingly
            if (randomNum.Next(10) < 5)
            {
                position = new Rectangle(0, 0, height, width);   
            }
            else
            {
                position = new Rectangle(0, 0, width, height);
            }

            return position;
        }      

        /// <summary>
        /// updates all the obstacles with player collision data
        /// </summary>
        /// <param name="player">the player you're using to update</param>
        public void UpdateWallCollisions(Player player)
        {
            //update all obstacles
            foreach (Obstacle obstacle in obstacles)
            {
                obstacle.CheckForCollision(player);
            }           
        }

        /// <summary>
        /// updates all the walls with enemy collision data
        /// </summary>
        /// <param name="enemies">the list of enemies to use for updates</param>
        public void UpdateWallCollisions(List<Enemy> enemies)
        {
            //check each enemy
            foreach (Enemy temp in enemies)
            {
                //only check obstacle collisions with chaser ghosts 
                if(temp.GetType() == typeof(ChaserGhost))      
                {
                    //check for collisions with all obstacles
                    foreach (Obstacle obstacle in obstacles)
                    {                        
                        obstacle.CheckForCollision(temp);
                    }
                } 
            }
        }

        /// <summary>
        /// delete any obstacles that have "expired"
        /// </summary>
        private void DeleteObstacles()
        {
            //check each obstacle, iterate backwards through the list
            for (int i = obstacles.Count - 1; i >= 0; i--)
            {
                if (obstacles[i].Timer >= deleteTime)
                {
                    obstacles.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// updates the direction of each wall, reverses the direction when they reach the edge
        /// </summary>
        /// <param name="obstacle">the obstacle to check</param>
        private void UpdateDirection(Obstacle obstacle)
        {
            if (obstacle.Position.X < 0 - obstacle.Position.Width)      //at left edge of the boudaries
            {
                obstacle.ReverseDirection();
            }
            else if (obstacle.Position.X > Boundaries.Width)            //at the right edge of the boundaries
            {
                obstacle.ReverseDirection();
            }
            else if (obstacle.Position.Y < 0 - obstacle.Position.Height)    //at the top edge of the boundaries
            {
                obstacle.ReverseDirection(); 
            }
            else if (obstacle.Position.Y > Boundaries.Height)           //at the bottom edge of the boundaries
            {
                obstacle.ReverseDirection();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Obstacle temp in obstacles)
            {
                temp.Draw(spriteBatch);
            }
        }

        /// <summary>
        /// assign the obstacle texture
        /// </summary>
        public Texture2D ObstacleTexture
        {
            set
            {
                obstacleTexture = value;
            }
        }

        /// <summary>
        /// set the height of the obstacles
        /// </summary>
        public int Height
        {
            set
            {
                if (height < 0)
                {
                    height = 50;
                }
                else
                {
                    height = value;
                }
            }
        }

        /// <summary>
        /// set the width of the obstacles
        /// </summary>
        public int Width
        {
            set
            {
                if (width < 0)
                {
                    width = 50;
                }
                else
                {
                    width = value;
                }
            }
        }

        /// <summary>
        /// retrieve the list of objects stored in the manager
        /// </summary>
        public List<Obstacle> ObstacleList
        {
            get
            {
                return obstacles;
            }
        }

        /// <summary>
        /// set the time you want the obstacles to delete after
        /// </summary>
        public float DeleteTimer
        {
            set
            {
                deleteTime = value;
            }
        }

        /// <summary>
        /// set the maximum speed of the obstacles being generated
        /// </summary>
        public float MaxObstacleSpeed
        {
            set
            {
                maxObstacleSpeed = value;
            }
        }
    }
}
