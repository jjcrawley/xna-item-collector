﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    class Character
    {
        protected Vector2 position;         //the position of the character       
        protected Vector2 heading;          //the heading vector of the character
                                            //it uses the vector to move

        protected int currentHealth;        //the current heatlh of the character
        protected float speed;              //the speed that the character moves at

        protected float orientation;        //the angle of orientation of the character
        protected float rotationSpeed;      //the speed that the character can turn
        protected float turnFactor;         //how much of the turn speed is the character allowed to use

        private Rectangle worldBounds;      //the boundaries that the character can move around in
        private Rectangle boundingBox;      //the bounding box of the character, used for collision detection

        private float scale;                //the scale of the character, used for drawing purposes 
        private Texture2D characterTexture; //the character texture
        private Color characterColour;      //the character colour
        private Vector2 textureCentre;      //the centre of the texture

        protected bool bounced;             //a bounced variable to say whether the object has been bounced or not

        public Character()
        { }

        /// <summary>
        /// create a character at a given position with a specified world size
        /// </summary>
        /// <param name="bounds">the boundries in which the character can wander</param>
        /// <param name="characterPos">the position of the character</param>
        public Character(Rectangle bounds, Vector2 characterPos)
        {
            worldBounds = bounds;

            position = characterPos;

            speed = 2.0f;

            Scale = 1.0f;

            heading = new Vector2(0, 0);
        }

        /// <summary>
        /// calculates the heading of the character
        /// </summary>
        protected void CalculateHeading()
        {
            heading = OrientationAsVector(orientation); 
        }

        /// <summary>
        /// converts an angle into its corresponding vector format
        /// </summary>
        /// <param name="angle">the angle to convert</param>
        /// <returns>the angle in vector format</returns>
        protected Vector2 OrientationAsVector(float angle)
        {
            Vector2 vectorOrientation;

            vectorOrientation.X = (float)Math.Cos(angle);
            vectorOrientation.Y = (float)Math.Sin(angle);

            return vectorOrientation;
        }

        /// <summary>
        /// bounces the character, it calculates a new orientation using a specified point
        /// </summary>
        /// <param name="bounceFrom">the point to bounce relative to</param>
        public virtual void Bounce(Point bounceFrom)
        {
            Vector2 bounceVector;

            bounceVector.X = bounceFrom.X;
            bounceVector.Y = bounceFrom.Y;

            bounceVector = position - bounceVector;

            //Console.WriteLine("Bounce Vector: " + bounceVector);

            float bounceOrien;      //the angle at which to bounce

            bounceOrien = (float)Math.Atan2(bounceVector.Y, bounceVector.X);

            //Console.WriteLine("Orientation" + orientation);

            Orientation = bounceOrien;

            //Console.WriteLine("Orientation new: " + orientation);

            heading = OrientationAsVector(orientation);     //calculate the characters new heading 

            position += heading * speed * 10;

            bounced = true;
        }

        /// <summary>
        /// bounce behvaiour that uses a box and intersections to determine whether it should change its orientation
        /// </summary>
        /// <param name="box"></param>
        public virtual void Bounce(Rectangle box)
        {
            position += heading * speed * 2;    //conduct a test

            if (BoundingBox.Intersects(box))    //if we're still intersecting, bounce the character and flip the orientation
            {                
                Orientation += (float)Math.PI;      //flip the orientation
                CalculateHeading();                 //calculate the new heading
                position += heading * speed * 10;   //update the position
                //Console.WriteLine("Orientation: " + orientation);  
                bounced = true;             
            }           
        }

        /// <summary>
        /// defualt boucnce behaviour, bounce the character wihtout re-orienting him
        /// </summary>
        public virtual void Bounce()
        {
            position += heading * -speed * 10;      //re-position character by reversing speed

            bounced = true;
        }       

        /// <summary>
        /// advanced collision behaviour for a square collider
        /// </summary>
        /// <param name="collider">the rectangle we're colliding with</param>
        public void Collide(Rectangle collider)
        {
            Rectangle bounds;
            bounds = BoundingBox;     //gets the bounding box of the character, uses an up to date value while minimizing calculations made

            //the bounds is being re-positioned relative to the collider rectangle
            if (bounds.Center.Y < collider.Top)             //at top edge of collider
            {                
                if (bounds.Center.X > collider.Right)       //at top right corner of collider
                {
                    //coming from above
                    if ((collider.Top - bounds.Center.Y) > (bounds.Center.X - collider.Right))   
                    {
                        bounds.Y = collider.Top - bounds.Height;    //re-position to top edge
                    }
                    else                                    //coming from the right
                    {
                        bounds.X = collider.Right;                  //re-position to right edge
                    }                   
                }
                else if (bounds.Center.X < collider.Left)    //at top left corner of collider
                {
                     //coming from top
                     if((collider.Top - bounds.Center.Y) > (collider.Left - bounds.Center.X))       
                     {
                         bounds.Y = collider.Top - bounds.Height;   //re-position to top edge
                     }
                     else                                     //coming from the left
                     {
                         bounds.X = collider.Left - bounds.Width;   //re-position to left edge
                     }
                }
                else 
                {
                    bounds.Y = collider.Top - bounds.Height;        //re-position to top edge
                }
            }
            else if (bounds.Center.Y > collider.Bottom)       //at bottom edge
            {
                if (bounds.Center.X < collider.Left)          //at bottom left corner
                {
                    if (collider.Left - bounds.Center.X > Math.Abs(collider.Bottom - bounds.Center.Y))  //coming from left
                    {
                        bounds.X = collider.Left - bounds.Width;        //re-position to left edge
                    }
                    else                                      //coming from below
                    {
                        bounds.Y = collider.Bottom;                     //re-position to bottom edge
                    }
                }
                else if (bounds.Center.X > collider.Right)    //near bottom right corner
                {
                    //coming from below
                    if (bounds.Center.Y - collider.Bottom > bounds.Center.X - collider.Right)   
                    {                        
                        bounds.Y = collider.Bottom;                 //re-position to bottom edge
                    }
                    else                                      //coming from the right
                    {
                        bounds.X = collider.Right;                  //re-position to right edge
                    }
                }
                else
                {
                    bounds.Y = collider.Bottom;
                }
            }
            else if (bounds.Center.X > collider.Right)        //at right edge
            {  
                bounds.X = collider.Right;                          //re-position to right edge
            }            
            else                                              //at the left edge
            {
                bounds.X = collider.Left - bounds.Width;            //re-position to left edge
            }

            //assign a new position for the character
            Vector2 rePosition;

            rePosition.X = bounds.X + bounds.Width / 2.0f;
            rePosition.Y = bounds.Y + bounds.Height / 2.0f;

            position = rePosition;      //re-position the character
            bounced = true;
        }

        /// <summary>
        /// wraps the world in a taurus
        /// </summary>
        protected void WrapViewPort()
        {
            //uses the bounding box to determine whether it should re-position
            //this makes the transition from one side of the screen to the other appear less jumpy
            if (BoundingBox.X < 0 - boundingBox.Width)      //exiting on the left side
            {
                position.X = worldBounds.Width + boundingBox.Width / 2.0f;
            }
            else if (BoundingBox.X > worldBounds.Width)     //exiting on the right side
            {
                position.X = 0 - boundingBox.Width / 2.0f;
            }

            if (BoundingBox.Y < 0 - boundingBox.Height)     //exiting from the top
            {
                position.Y = worldBounds.Height + boundingBox.Height / 2.0f;
            }
            else if (BoundingBox.Y > worldBounds.Height)    //exiting from the bottom
            {
                position.Y = 0 - boundingBox.Height / 2.0f;
            }
        } 

        /// <summary>
        /// set and get the orientation of the character
        /// </summary>
        public float Orientation
        {
            set
            {
                orientation = value;

                orientation = WrapAngle(orientation);
            }
            get
            {
                return orientation;
            }
        }

        /// <summary>
        /// wraps a given angle, i.e: keeps it in the range of -PI to +PI
        /// </summary>
        /// <param name="angle">the angle to wrap</param>
        /// <returns>the wrapped angle</returns>
        protected float WrapAngle(float angle)
        {
            while (angle < -MathHelper.Pi)
            {
                angle += MathHelper.TwoPi;
            }
            while (angle > MathHelper.Pi)
            {
                angle -= MathHelper.TwoPi;
            }

            return angle;
        }

        /// <summary>
        /// draw the character
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            textureCentre.X = characterTexture.Width / 2.0f;
            textureCentre.Y = characterTexture.Height / 2.0f;

            spriteBatch.Draw(characterTexture, position, null, characterColour, orientation, textureCentre, scale, SpriteEffects.None, 0.0f);

            //RotatingProjectWithShapes.Primitives2D.DrawRectangle(spriteBatch, BoundingBox, Color.Red);
        }       

        /// <summary>
        /// get the collision box for the character
        /// </summary>
        public Rectangle BoundingBox
        {
            get
            {
                //calculate the width and height of the bounding box
                boundingBox.Width = (int)(scale * (Math.Abs(characterTexture.Width * Math.Cos(orientation)) + Math.Abs(characterTexture.Height * Math.Sin(orientation))));
                boundingBox.Height = (int)(scale * (Math.Abs(characterTexture.Height * Math.Cos(orientation)) + Math.Abs(characterTexture.Width * Math.Sin(orientation))));

                //using the newly calculated height and width 
                //position the boundary box around the character position
                boundingBox.X = (int)(position.X - boundingBox.Width / 2.0f);
                boundingBox.Y = (int)(position.Y - boundingBox.Height / 2.0f);

                return boundingBox;
            } 
        }

        /// <summary>
        /// set the character texture
        /// </summary>
        public Texture2D CharacterTexture
        {
            set
            {
                characterTexture = value;
            }            
        }

        /// <summary>
        /// set the character colour
        /// </summary>
        public Color CharacterColour
        {
            set
            {
                characterColour = value;
            }           
        }        

        /// <summary>
        /// set the size of the character
        /// </summary>
        public float Scale
        {
            set
            {
                if (value <= 0)
                {
                    scale = 1.0f;   //defualt scale
                }
                else
                {
                    scale = value;
                }
            }
        }

        /// <summary>
        /// set the speed of the character
        /// a negative speed will cause the character to move in reverse
        /// </summary>
        public float Speed
        { 
            set
            {
                speed = value;
            }
            get
            {
                return speed; 
            }
        }

        /// <summary>
        /// set and retrieve the position of the character
        /// </summary>
        public Vector2 Position
        {
            set
            {
                position = value;
            }
            get
            {
                return position;
            }
        }

        /// <summary>
        /// set and retrieve the current health of the character
        /// </summary>
        public virtual int Health
        {
            set
            {
                currentHealth = value;
            }
            get
            {
                return currentHealth;
            }
        }

        /// <summary>
        /// set and retrieve the rotation speed of the character
        /// </summary>
        public float RotationSpeed
        {
            set
            {
                rotationSpeed = value;                
            }
            get
            {
                return rotationSpeed;
            }
        }

        /// <summary>
        /// set and retrieve the turn facter of the character
        /// </summary>
        public float TurnFactor
        {
            set
            {
                turnFactor = value;
            }
            get
            {
                return turnFactor;
            }
        }
    }
}
