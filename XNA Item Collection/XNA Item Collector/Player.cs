﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the player class
    /// the player is capable of moving and rotating
    /// </summary>
    class Player : Character
    {
        private int maxHealth;      //the maximum health of the player        
        private int wealth;         //the current amount of wealth that the player has accumulated       

        private TextBox healthHUD;  //a textbox used to display the players health
        private TextBox wealthHUD;  //a textbox used to display the players wealth

        public Player()
        {
 
        }

        /// <summary>
        /// a player with custom position and wandering boundaries
        /// </summary>
        /// <param name="pos">the position of the player</param>
        /// <param name="wanderingBounds">the wandering boundaries</param>
        public Player(Vector2 pos, Rectangle wanderingBounds) 
            : base(wanderingBounds, pos)
        {
            orientation = 0.0f;

            maxHealth = 20;         //setting up the player health stats
            currentHealth = maxHealth;
            wealth = 0;            

            TurnFactor = 0.01f;              //default turn factor value
            RotationSpeed = turnFactor * 5.0f;          //defualt rotation speed  

            CharacterColour = Color.Red;

            //create the character HUD elements
            //tweaking the position of temp will affect the position of the HUD elements
            Vector2 temp;

            temp.X = 20;
            temp.Y = 20;

            //create a new health HUD element with a set position and start message
            healthHUD = new TextBox(temp, "Health: " + currentHealth);
            healthHUD.Scale = 1.0f;

            temp.Y += 40;

            //create a wealth HUD element with a set position and start message 
            wealthHUD = new TextBox(temp, "Wealth: " + wealth);
            wealthHUD.Scale = 1.0f;
        }

        /// <summary>
        /// defualt update behaviour
        /// </summary>
        /// <param name="press">the current keyboard state</param>
        public void Update(KeyboardState press)
        {
            CharacterControls(press);

            WrapViewPort();
        }

        /// <summary>
        /// handles the controls for the player
        /// </summary>
        /// <param name="current">the current key board state</param>
        private void CharacterControls(KeyboardState current)
        {
            //rotate the character
            if (current.IsKeyDown(Keys.A))
            {
                Orientation -= rotationSpeed;       //rotate anti-clockwise 
            }
            else if (current.IsKeyDown(Keys.D))
            {
                Orientation += rotationSpeed;       //rotate clockwise
            }

            //move the character
            if (current.IsKeyDown(Keys.W))
            {
                MoveForward(); 
            }
            else if (current.IsKeyDown(Keys.S))
            {
                MoveBackward();                
            }            

            UpdateHealthHUD();
            UpdateWealthHUD();
        }

        /// <summary>
        /// update the player health HUD element
        /// </summary>
        public void UpdateHealthHUD()
        {
            healthHUD.Text = "Health: " + currentHealth;
        }

        /// <summary>
        /// update the player wealth HUD element
        /// </summary>
        public void UpdateWealthHUD()
        {
            wealthHUD.Text = "Wealth: " + wealth;
        }

        /// <summary>
        /// moves the game object backwards
        /// </summary>
        private void MoveBackward()
        {
            CalculateHeading();

            heading.Normalize();

            position -= heading * speed;
        }

        /// <summary>
        /// moves the game object forwards
        /// </summary>
        private void MoveForward()
        {
            CalculateHeading();

            heading.Normalize();

            position += heading * speed;            
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            healthHUD.Draw(spriteBatch);
            wealthHUD.Draw(spriteBatch);

            base.Draw(spriteBatch);
        }

        /// <summary>
        /// set and retrieve the current player wealth
        /// </summary>
        public int Wealth
        {
            set
            {
                wealth = value;
                //Console.WriteLine("Wealth: " + wealth);
            }
            get
            {
                return wealth;
            }
        }

        /// <summary>
        /// set the max health of the character
        /// <remarks>if the value is less than zero then the max health will default to 20</remarks>
        /// </summary>
        public int MaxHealth
        {
            set
            {
                maxHealth = value;

                if (maxHealth < 0)
                {
                    maxHealth = 20;         //default health
                }
            }
        }

        /// <summary>
        /// set and retrieve the players current health
        /// </summary>
        public override int Health
        {
            set
            {
                currentHealth = value;

                if (currentHealth > maxHealth)      //stops current health from exceeding max health
                {
                    currentHealth = maxHealth;
                }
                else if (currentHealth < 0)         //stops current health from going less than zero
                {
                    currentHealth = 0;
                }

                //Console.WriteLine("Current Health: " + currentHealth);
            }
            get
            {
                return currentHealth;
            }
        }

        /// <summary>
        /// set the player HUD fonts
        /// </summary>
        public SpriteFont HUDFonts
        {
            set
            {
                healthHUD.Font = value;
                wealthHUD.Font = value;
            }
        }
    }
}
