﻿using System;
using Microsoft.Xna.Framework;

namespace SOFT144_Assignment
{
    /// <summary>
    /// the manager class
    /// this class provides methods to create and update a stop clock
    /// stores the bounds for the manager
    /// provides a random number generator for any sub-class
    /// </summary>
    class Manager
    {
        protected Random randomNum;         
        private Rectangle boundaries;       //the boundaries that the objects can spawn within 

        private float timer;                //a basic timer
        private float spawnTimer;           //the time for the next spawn

        private int limit;                  //the maximum number of objects within the scene

        /// <summary>
        /// create a defualt manager
        /// </summary>
        public Manager()
        {
            randomNum = new Random(); 
        }

        /// <summary>
        /// create a defualt manager with custom boundaries
        /// </summary>
        /// <param name="bounds">the custom boundaries</param>
        public Manager(Rectangle bounds)
        {
            boundaries = bounds;
            randomNum = new Random();
        }

        /// <summary>
        /// adds time to the timer, causes it to 'tick'
        /// </summary>
        /// <param name="gameTime">the current gametime</param>
        protected void TimerTick(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds; 
        }

        /// <summary>
        /// resets the timer
        /// </summary>
        protected void ResetTimer()
        {
            timer = 0;
        }

        /// <summary>
        /// randomises the x position of a target rectangle
        /// </summary>
        /// <param name="keepWithinBounds">generate a value within the boundaries, or set it to the left or right edge</param>
        /// <param name="targetRectangle">the target rectangle</param>
        /// <returns>the newly randomised x co-ord</returns>
        public int RandomiseXPosition(bool keepWithinBounds, Rectangle targetRectangle)
        {
            //generate values within the boundaries
            if (keepWithinBounds)
            {
                targetRectangle.X = randomNum.Next(Boundaries.Width - targetRectangle.Width);
            }
            else       //otherwise, place on either the left or right edge
            {
                int leftOrRight;

                leftOrRight = randomNum.Next(2);

                if (leftOrRight == 1)       //place on the left, generates off map
                {
                    targetRectangle.X = 0 - targetRectangle.Width;
                }
                else            //place on the right edge
                {
                    targetRectangle.X = Boundaries.Width;
                }
            }

            return targetRectangle.X;
        }

        /// <summary>
        /// randomises the y position of a target rectangle
        /// </summary>
        /// <param name="keepWithinBounds">generate values within the boundaries, or set it to the top or bottom edge</param>
        /// <param name="targetRectangle">the target rectangle</param>
        /// <returns>the newly randomised y co-ord</returns>
        public int RandomiseYPosition(bool keepWithinBounds, Rectangle targetRectangle)
        {
            //generates a value within the bounds
            if (keepWithinBounds)
            {
                targetRectangle.Y = randomNum.Next(Boundaries.Height - targetRectangle.Height);
            }
            else        //or place on either the top or bottom edge
            {
                int aboveOrBelow;

                aboveOrBelow = randomNum.Next(2);

                if (aboveOrBelow == 1)      //place on the top edge
                {
                    targetRectangle.Y = 0 - targetRectangle.Height;
                }
                else                        //place on the bottom edge
                {
                    targetRectangle.Y = Boundaries.Height;
                }
            }

            return targetRectangle.Y;
        }

        /// <summary>
        /// get the current time on the timer
        /// </summary>
        public float Timer
        {
            get
            {
                return timer;
            }
        }

        /// <summary>
        /// set the box in which the objects are allowed to spawn within
        /// </summary>
        public Rectangle Boundaries
        {
            set
            {
                boundaries = value;
            }
            get
            {
                return boundaries;
            }
        }

        /// <summary>
        /// this sets the number of particular objects are allowed to be in the scene at any one time
        /// </summary>
        public int Limit
        {
            set
            {
                if (value < 0)
                {
                    limit = 0;
                }
                else
                {
                    limit = value;
                }
            }
            get
            {
                return limit;
            }
        }

        /// <summary>
        /// set and get the next spawn time
        /// </summary>
        public float SpawnTimer
        {
            set
            {
                spawnTimer = value;
            }
            get
            {
                return spawnTimer;
            }
        }
    }
}
