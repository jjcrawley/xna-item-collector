﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SOFT144_Assignment
{
    /// <summary>
    /// creates a simple text box
    /// </summary>
    class TextBox
    {
        private string text;        //the message that goes in the text box
        private SpriteFont font;    //the font 
        private Color colour;       //the colour of the text
        private float scale;        //the scale of the text box
        private float rotation;     //the rotation of the textbox

        private Vector2 position;       //the position of the box
        private Vector2 centre;         //the position around which to rotate the text box

        public TextBox()
        {
            font = null;
            text = null;
            centre = new Vector2(0, 0);
            scale = 1.0f;
            rotation = 0;
        }

        public TextBox(string startText)
        {
            font = null;
            text = startText;
            colour = Color.Black;
            centre = new Vector2(0, 0);
            scale = 1.0f;
            rotation = 0;
        }

        /// <summary>
        /// create a simple text box with a start message at a specified location
        /// <remarks>you'll need to set the font type</remarks>
        /// </summary>
        /// <param name="textBoxPos">the position of the text box</param>
        /// <param name="startMessage">the starting message</param>
        public TextBox(Vector2 textBoxPos, string startMessage)
        {
            text = startMessage;
            position = textBoxPos;
            colour = Color.Black;
            centre = new Vector2(0,0);
            scale = 1.0f;
            rotation = 0;
        }        

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position, colour, rotation, centre, scale, SpriteEffects.None, 0.0f);
        }

        /// <summary>
        /// set the scale of the font in the text box
        /// </summary>
        /// <remarks>defualt scale is 1</remarks>
        public float Scale
        {
            set
            {
                scale = value;

                if (scale <= 0)
                {
                    scale = 1.0f;
                }
            }
        }

        /// <summary>
        /// set the rotation of the text box
        /// </summary>
        public float Rotation
        {
            set
            {
                rotation = value;
            }
            get
            {
                return rotation;
            }
        }

        /// <summary>
        /// set and retrieve the position of the text box
        /// </summary>
        public Vector2 Position
        {
            set
            {
                position = value;
            }
            get
            {
                return position;
            }
        }

        /// <summary>
        /// set the text colour
        /// </summary>
        public Color FontColour
        {
            set
            {
                colour = value;
            }
        }

        /// <summary>
        /// set the font of the text
        /// </summary>
        public SpriteFont Font
        {
            set
            {
                font = value;
            }
        }

        /// <summary>
        /// set or retrieve the message in the text box
        /// </summary>
        public string Text
        {
            set
            {
                text = value;            
            }
            get
            {
                return text;
            }
        }

        /// <summary>
        /// set and retrieve the centre position of the text box
        /// </summary>
        public Vector2 Centre
        {
            set
            {
                centre = value;
            }
            get
            {
                return centre;
            }
        }
    }
}
